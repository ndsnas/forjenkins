package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller
{
@Autowired	
private Service service;
@GetMapping("/get")
public List<Integer> list()
{
return service.getList();
}
@GetMapping("/get/{id}")
public Integer getListById(@PathVariable int id)
{
return service.getListById(id);
}

}
